const webpack = require('webpack');
const debug = true;
const devtool = 'source-map';

module.exports = [
    {
        debug,
        devtool,
        entry: {
            theajax: [ `${__dirname}/src/theajax.ts` ]
        },
        output: {
            filename: 'theajax.js',
            path: `${__dirname}/dist/`
        },
        module: {
            loaders: [
                {
                    test: /\.tsx?/i,
                    exclude: /node_modules/,
                    loader: 'ts-loader'
                }
            ]
        },
        resolve: {
            extensions: ['', '.ts', '.js'],
            moduleDirectories: ['node_modules']
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.UglifyJsPlugin({
                compressor: {
                    warnings: false
                }
            })
        ]
    }
];