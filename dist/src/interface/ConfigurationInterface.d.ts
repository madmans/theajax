export interface Request {
    method?: RequestMethod;
    headers?: RequestHeaders;
    body?: RequestBody;
    mime?: string;
    mode?: RequestMode;
    credentials?: RequestCredentials;
    cache?: RequestCache;
    redirect?: RequestRedirect;
    referrer?: RequestReferrer;
    referrerPolicy?: RequestReferrerPolicy;
    integrity?: string;
}
export default Request;
