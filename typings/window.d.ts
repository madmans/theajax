import ConfigurationInterface from './../src/interface/ConfigurationInterface'
import RequestPromiseInterface from './../src/request/RequestPromiseInterface';
import ResponseInterface from './../src/response/ResponseInterface';
import FederatedCredential from './../src/interface/FederatedCredential';
import PasswordCredential from './../src/interface/PasswordCredential';

declare global {
    type RequestMethod           = 'get' | 'head' | 'post' | 'put' | 'delete' | 'trace' | 'options' | 'connect' | 'patch';
    type RequestHeaders          = { [key: string]: string };
    type RequestBody             = FormData | Object | string;
    type RequestMode             = 'cors' | 'no-cors' | 'same-origin' | string;
    type RequestCredentials      = 'omit' | 'same-origin' | 'include' | FederatedCredential | PasswordCredential;
    type RequestCache            = 'default' | 'no-store' | 'reload' | 'no-cache' | 'force-cache' | 'only-if-cached';
    type RequestRedirect         = 'follow' | 'error' | 'manual';
    type RequestReferrer         = 'no-referrer' | 'client' | string;
    type RequestReferrerPolicy   = 'no-referrer' | 'no-referrer-when-downgrade' | 'origin' | 'origin-when-cross-origin' | 'unsafe-url';

    interface Window {
        theajax(url: string, configuration: ConfigurationInterface): RequestPromiseInterface<ResponseInterface>;
    }

    interface FormData {
        values(): Array<any>
    }
}