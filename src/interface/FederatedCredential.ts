import Credential from './Credential';

export interface FederatedCredential extends Credential {
    provider: string;
}

export default FederatedCredential;