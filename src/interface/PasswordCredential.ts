import Credential from './Credential';
import URLSearchParams from './URLSearchParams';

interface PasswordCredential extends Credential {
    additionalData: FormData | URLSearchParams | null;
    idName: string;
    passwordName: string;
}

export default PasswordCredential;