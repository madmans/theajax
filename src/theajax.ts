import ConfigurationInterface from './interface/ConfigurationInterface';
import Response from './response/Response';
import ResponseInterface from './response/ResponseInterface';
import RequestPromise from './request/RequestPromise';

/// <reference types="./../typings/window.d.ts" />

const theajax = function theajax(url: string, configuration: ConfigurationInterface = {}): RequestPromise<ResponseInterface> {
    const xhr = new XMLHttpRequest();

    const promise = new RequestPromise<ResponseInterface>((resolve, reject) => {
        let method = String(configuration.method || 'get').trim().toLowerCase();

        // Check body
        let body: RequestBody = undefined;
        let mime = undefined;
        // user passed form data
        if ((configuration.body as any) instanceof FormData) {
            body = (configuration.body as FormData);

            let hasFile = false;
            for (let value of (body as FormData).values()) {
                if (value instanceof File || value instanceof FileList) {
                    hasFile = true;
                    break;
                }
            }

            if (hasFile) {
                mime = 'multipart/form-data';
            } else {
                mime = 'application/x-www-form-urlencoded';
            }
        // user passed object that should be stringify and send as json
        } else if ((configuration.body as any) instanceof Object) {
            body = JSON.stringify(configuration.body);
            mime = 'application/json';
        // user passed something else
        } else {
            body = configuration.body;
        }

        if (body !== undefined && method !== 'post' && method !== 'put') {
            throw new Error(`Invalid method. You cannot send request with body using "${method}" method`);
        }

        // Set requests;
        const responseHeaders: { [ key: string ]: string } = {};
        for (let header in configuration.headers) {
            let value = configuration.headers[header];
            xhr.setRequestHeader(header, value);
        }

        // In case mime is defined
        if (configuration.mime || mime) {
            xhr.overrideMimeType(configuration.mime || mime);
        }

        xhr.onreadystatechange = function () {
            switch (this.readyState) {
                case XMLHttpRequest.HEADERS_RECEIVED:
                    const headerList = this.getAllResponseHeaders().split('\r\n');
                    headerList.forEach((item: string) => {
                        const split = item.split(':');
                        if (split.length === 2) {
                            responseHeaders[String(split[0]).trim()] = String(split[1]).trim();
                        }
                    });
                    break;
                case XMLHttpRequest.DONE:
                    const response = new Response(
                        responseHeaders,                                // headers
                        this.status,                                    // status
                        this.statusText,                                // status text
                        this.responseType,                              // mode
                        this.responseURL,                               // url
                        this.responseText                               // body
                    );

                    if (this.status >= 200 && this.status < 300) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                    break;
            }
        };

        xhr.open(method, url);
        xhr.send(body || undefined);
    }, xhr);

    return promise;
};

if (window !== undefined) {
    if (window['theajax'] === undefined) {
        window['theajax'] = theajax;
    }
}


export default theajax;