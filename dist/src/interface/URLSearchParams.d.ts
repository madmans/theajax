export interface URLSearchParams {
    append(name: string, value: string | number): void;
    delete(name: string): void;
    entries(): any;
    get(name: string): string | number;
    getAll(name: string): Array<string>;
    has(name: string): boolean;
    keys(): any;
    set(name: string, value: string | number): void;
    toString(): string;
    values(): any;
}
export default URLSearchParams;
