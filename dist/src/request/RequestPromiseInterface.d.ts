/// <reference types="es6-promise" />
interface RequestPromiseInterface<T> extends Thenable<T> {
    abort(): void;
}
export default RequestPromiseInterface;
