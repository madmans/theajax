export interface Credential {
    iconURL: string;
    id: string;
    name: string;
    type: string;
}

export default Credential;