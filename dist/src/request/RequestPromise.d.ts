/// <reference types="es6-promise" />
import { Promise } from 'es6-promise';
import RequestPromiseInterface from './RequestPromiseInterface';
export declare class RequestPromise<T> extends Promise<T> implements RequestPromiseInterface<T> {
    xhr: XMLHttpRequest;
    constructor(callback: (resolve: (value?: T | Thenable<T>) => void, reject: (error?: any) => void) => void, xhr: XMLHttpRequest);
    abort(): void;
}
export default RequestPromise;
