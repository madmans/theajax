export interface ResponseInterface {
    readonly headers: {
        [key: string]: string;
    };
    readonly ok: boolean;
    readonly redirected: boolean;
    readonly status: number;
    readonly statusText: string;
    readonly type: string;
    readonly url: string;
}
export default ResponseInterface;
