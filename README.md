# THE AJAX

Simple Ajax library. Still work in progress. This should NOT be used in production.

> **Attention!**
> Library just appears like one of the [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) polyfills but it's NOT. I'm not planning to stick with fetch API.
> As example, fetch return promises. theajax returns promise-like object with support for .abort().

## INSTALLATION

```
npm install theajax --save
```

or if You are using Yarn

```
yarn add theajax
```

## USAGE

To use library, first add it to your project.

#### Adding script or importing using ES6 modules

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <script src="/path/to/theajax.js"></script>
</body>
</html>
```

This will expose global variable in case "window" is not undefined called "theajax". So from now one you have global variable "theajax".

If you are using some kind of package manager (like webpack) you can also install theajax via npm/yarn and import it as module:

```
import theajax from "theajax";
```

#### Making request

```
import theajax from "theajax";

theajax('https://api.wheretheiss.at/v1/satellites/25544').then(() => {
    document.write();
});
```

#### Changing method

```
import theajax from "theajax";

theajax('https://example.com/v1/user', {
    method: 'post',
    body: {
        name: 'Dariusz'
    }
}).then(() => {
    document.write();
});
```

#### Seinding form

You can send FormData, JSON or plain text.

```
import theajax from "theajax";

let data = new FormData();
data.append('name', 'Dariusz');

theajax('https://example.com/v1/user', {
    method: 'post',
    body: data
}).then((response) => {
    console.log(response);
});
```

#### Aborting requests

Sometimes you need to abort your request for some reason. theajax return promise-like object that gives you ability to do just that.

```
theajax('https://api.wheretheiss.at/v1/satellites/25544').abort();
```

### IMPLMENETED:

* send plain text, json object or form data
* use different method types
* very simple autodiscovery of mime type
* ability to overwrite request mime type
* aborting requests

#### TODO:

* support for mode, credentials, cache, redirect, referrer and integrity
* tests
* browser support for IE and mobile browsers
* more tests
* better support for errors, timeouts etc