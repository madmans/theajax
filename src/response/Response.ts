import ResponseInterface from './ResponseInterface';

export class Response implements ResponseInterface {
    readonly headers: RequestHeaders;
    readonly ok: boolean;
    readonly redirected: boolean;
    readonly status: number;
    readonly statusText: string;
    readonly type: string;
    readonly url: string;
    readonly body: string;

    constructor(headers: RequestHeaders, status: number, statusText: string, type: RequestMode, url: string, body: string) {
        this.headers = headers;
        this.status = status;
        this.statusText = statusText;
        this.type = type;
        this.url = url;
        this.body = body;
    }

    getHeaders(): Object {
        return this.headers;
    }

    getHeader(name: string): string {
        return this.headers[name] || '';
    }

    hasHeader(name: string): boolean {
        return this.headers[name] !== undefined;
    }

    getStatus(): number {
        return this.status;
    }

    getStatusText(): string {
        return this.statusText;
    }

    getType(): RequestMode {
        return this.type;
    }

    getMode(): RequestMode {
        return this.getType();
    }

    getUrl() {
        return this.url;
    }

    getBody(): string {
        return this.body;
    }

    text(): string {
        return this.body;
    }

    json(): Object {
        return JSON.parse(this.body);
    }

    clone() {
        return new Response(this.headers, this.status, this.statusText, this.type, this.url, this.body);
    }
}

export default Response;