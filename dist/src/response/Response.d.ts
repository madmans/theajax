import ResponseInterface from './ResponseInterface';
export declare class Response implements ResponseInterface {
    readonly headers: RequestHeaders;
    readonly ok: boolean;
    readonly redirected: boolean;
    readonly status: number;
    readonly statusText: string;
    readonly type: string;
    readonly url: string;
    readonly body: string;
    constructor(headers: RequestHeaders, status: number, statusText: string, type: RequestMode, url: string, body: string);
    getHeaders(): Object;
    getHeader(name: string): string;
    hasHeader(name: string): boolean;
    getStatus(): number;
    getStatusText(): string;
    getType(): RequestMode;
    getMode(): RequestMode;
    getUrl(): string;
    getBody(): string;
    text(): string;
    json(): Object;
    clone(): Response;
}
export default Response;
