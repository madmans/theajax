import ConfigurationInterface from './interface/ConfigurationInterface';
import ResponseInterface from './response/ResponseInterface';
import RequestPromise from './request/RequestPromise';
declare const theajax: (url: string, configuration?: ConfigurationInterface) => RequestPromise<ResponseInterface>;
export default theajax;
