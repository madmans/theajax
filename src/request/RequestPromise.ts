import { Promise } from 'es6-promise';
import RequestPromiseInterface from './RequestPromiseInterface';

export class RequestPromise<T> extends Promise<T> implements RequestPromiseInterface<T> {
    xhr: XMLHttpRequest;

    constructor(callback: (resolve: (value?: T | Thenable<T>) => void, reject: (error?: any) => void) => void, xhr: XMLHttpRequest) {
        super(callback);
        this.xhr = xhr;
    }

    abort() {
        this.xhr.abort();
    }
}

export default RequestPromise;